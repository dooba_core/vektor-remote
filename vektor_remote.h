/* Dooba SDK
 * Stickmote-based remote controller for the Vektor
 */

#ifndef	__VEKTOR_REMOTE_H
#define	__VEKTOR_REMOTE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <socket/socket.h>
#include <pwm/heartbeat.h>
#include <stickmote/stickmote.h>

// Vektor WiFi AP Prefix
#define	VEKTOR_REMOTE_AP_PREFIX						"VKT:"

// Control Socket Address & Port
#define	VEKTOR_REMOTE_ADDR							"10.0.100.1"
#define	VEKTOR_REMOTE_PORT							14455

// Heartbeat shortcuts
#define vektor_remote_heartbeat_offline()			pwm_heartbeat_init(&vektor_remote_heartbeat, STICKMOTE_LED, 5000, 0x7f, 10000, 0x00, 50)
#define vektor_remote_heartbeat_online()			pwm_heartbeat_init(&vektor_remote_heartbeat, STICKMOTE_LED, 2000, 0xff, 5000, 0x3f, 100)
#define vektor_remote_heartbeat_error()				pwm_heartbeat_init(&vektor_remote_heartbeat, STICKMOTE_LED, 1000, 0xff, 1000, 0x00, 10)

// Socket
extern struct socket *vektor_remote_sock;

// LED Heartbeat
extern struct pwm_heartbeat vektor_remote_heartbeat;

// Track Values
extern int vektor_remote_track_l;
extern int vektor_remote_track_r;

// Main Init
extern void init();

// Main Loop
extern void loop();

// Socket Receive Handler
extern void vektor_remote_sock_recv(void *user, struct socket *s, uint8_t *data, uint16_t size);

// Compute Tracks
extern void vektor_remote_compute_tracks();

#endif

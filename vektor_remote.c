/* Dooba SDK
 * Stickmote-based remote controller for the Vektor
 */

// External Includes
#include <math.h>
#include <ard/ard.h>
#include <nomad/nomad.h>
#include <yolk/util/sbar.h>
#include <yolk/components/dialog.h>

// Internal Includes
#include "substrate.h"
#include "ap_menu.h"
#include "drv_dialog.h"
#include "vektor_remote.h"

// Socket
struct socket *vektor_remote_sock;

// LED Heartbeat
struct pwm_heartbeat vektor_remote_heartbeat;

// Joystick Calibration
uint8_t vektor_remote_cx;
uint8_t vektor_remote_cy;

// Track Values
int vektor_remote_track_l;
int vektor_remote_track_r;

// Status Bar
struct yolk_sbar vektor_remote_ysb;

// Main Init
void init()
{
	// Init Substrate
	substrate_init();

	// Clear Socket
	vektor_remote_sock = 0;

	// Setup Heartbeat
	vektor_remote_heartbeat_offline();

	// Attach Status Bar & Hook Power State to Nomad
	yolk_sbar_attach(&vektor_remote_ysb, ui0, YOLK_SBAR_POS_TOP);
	yolk_sbar_set_pwr_callbacks(&vektor_remote_ysb, (yolk_sbar_get_bat_pct_t)nomad_get_bat_pct, (yolk_sbar_is_charging_t)nomad_is_charging, bat0);

	// Get Stick Center Point
	vektor_remote_cx = ard(STICKMOTE_STICK_X);
	vektor_remote_cy = ard(STICKMOTE_STICK_Y);

	// Drive Dialog Inactive
	vektor_remote_drive_dialog_active = 0;

	// Enter AP Menu
	vektor_remote_ap_menu_enter(ui0);
}

// Main Loop
void loop()
{
	// Update Substrate
	substrate_loop();

	// Update Heartbeat
	pwm_heartbeat_update(&vektor_remote_heartbeat);
}

// Socket Receive Handler
void vektor_remote_sock_recv(void *user, struct socket *s, uint8_t *data, uint16_t size)
{
	// NoOp
}

// Compute Tracks
void vektor_remote_compute_tracks()
{
	double x;
	double y;
	double a;
	double ar;
	double al;
	double d;
	double l;
	double r;

	// Read Stick Position
	x = ((int)ard(STICKMOTE_STICK_X)) - ((int)vektor_remote_cx);
	y = ((int)ard(STICKMOTE_STICK_Y)) - ((int)vektor_remote_cy);

	// Determine angle & length
	a = atan2(x, y);
	d = sqrt((x * x) + (y * y));

	// Normalize Distance
	if(d < (((double)vektor_remote_cx) / 5))								{ d = 0.0; }

	// Compute Right
	ar = a;
	if(ar < 0)																{ ar = (M_PI * 2) + ar; }
	r = (ar > M_PI) ? (ar - M_PI) : ar;
	r = (r < (M_PI_2 + M_PI_4)) ? (1 - ((r / (M_PI_2 + M_PI_4)) * 2)) : -1.0;
	if(ar > M_PI)															{ r = -r; }

	// Compute Left
	al = -a;
	if(al < 0)																{ al = (M_PI * 2) + al; }
	l = (al > M_PI) ? (al - M_PI) : al;
	l = (l < (M_PI_2 + M_PI_4)) ? (1 - ((l / (M_PI_2 + M_PI_4)) * 2)) : -1.0;
	if(al > M_PI)															{ l = -l; }

	// Compute & Normalize Track Values
	r = r * d;
	if(r > 128.0)															{ r = 128.0; }
	if(r < -128.0)															{ r = -128.0; }
	l = l * d;
	if(l > 128.0)															{ l = 128.0; }
	if(l < -128.0)															{ l = -128.0; }
	vektor_remote_track_r = r;
	vektor_remote_track_l = l;
	if(abs(vektor_remote_track_r) < 10)										{ vektor_remote_track_r = 0; }
	if(abs(vektor_remote_track_l) < 10)										{ vektor_remote_track_l = 0; }
}

/* Dooba SDK
 * Stickmote-based remote controller for the Vektor
 */

// External Includes
#include <scli/scli.h>
#include <yolk/components/dialog.h>

// Internal Includes
#include "vektor_remote.h"
#include "ap_menu.h"
#include "drv_dialog.h"

// Drive Dialog Active
uint8_t vektor_remote_drive_dialog_active;

// Enter Drive Dialog
void vektor_remote_drv_dialog_enter(struct yolk *yk, char *name, uint8_t name_len)
{
	// Enter
	yolk_dialog_enter(yk, vektor_remote_drv_dialog_update, 0, YOLK_ICON_NET_HOME, "%t", "Online", name, name_len);
	vektor_remote_drive_dialog_active = 1;
}

// Update Drive Dialog
void vektor_remote_drv_dialog_update(struct yolk *yk, void *u)
{
	int d[2];

	// Handle Button
	if(yolk_in_center(yk))																							{ vektor_remote_ap_menu_enter(yk); return; }

	// Update Tracks
	vektor_remote_compute_tracks();

	// Send Tracks
	d[0] = vektor_remote_track_r;
	d[1] = vektor_remote_track_l;
	socket_send(vektor_remote_sock, d, sizeof(int) * 2);
}

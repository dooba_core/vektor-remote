/* Dooba SDK
 * Stickmote-based remote controller for the Vektor
 */

// External Includes
#include <net/ip.h>
#include <util/str.h>
#include <esp8266/esp8266.h>
#include <yolk/components/dialog.h>
#include <yolk/components/menu.h>

// Internal Includes
#include "substrate.h"
#include "vektor_remote.h"
#include "drv_dialog.h"
#include "ap_menu.h"

// AP List
struct esp8266_ap vektor_remote_ap_menu_list[VEKTOR_REMOTE_AP_MENU_LIST_SIZE];

// Enter AP Menu
void vektor_remote_ap_menu_enter(struct yolk *yk)
{
	uint8_t count;
	uint8_t i;
	struct esp8266_ap *ap;

	// Set heartbeat
	vektor_remote_heartbeat_offline();

	// Drive Dialog Inactive
	vektor_remote_drive_dialog_active = 0;

	// Clear Socket
	if(vektor_remote_sock)																										{ socket_close(vektor_remote_sock); }
	vektor_remote_sock = 0;

	// Inform
	yolk_dialog_enter(yk, 0, 0, YOLK_ICON_NET_UNKNOWN, "Please wait", "Searching for networks...");
	yolk_ui_draw(yk);

	// List AP
	if(esp8266_list_ap(vektor_remote_ap_menu_list, VEKTOR_REMOTE_AP_MENU_LIST_SIZE, &count))									{ yolk_dialog_enter_std_btn(yk, 0, YOLK_DIALOG_STD_BTN_OK, vektor_remote_ap_menu_retry, 0, 0, YOLK_ICON_NET_NONE, "Scan failed", "Press Ok to retry"); return; }

	// Build menu
	yolk_menu_enter(yk, vektor_remote_ap_menu_event, 0, YOLK_ICON_NET_UNKNOWN, "Select Vektor");
	for(i = 0; i < count; i = i + 1)
	{
		// Check device
		ap = &(vektor_remote_ap_menu_list[i]);
		if(str_starts_with(ap->name, ap->name_len, VEKTOR_REMOTE_AP_PREFIX))													{ yolk_menu_add_item(yk, i, YOLK_ICON_NET_HOME, "%t", ap->name, ap->name_len); }
	}
}

// Retry AP Menu
void vektor_remote_ap_menu_retry(struct yolk *yk, void *user)
{
	// Enter
	vektor_remote_ap_menu_enter(yk);
}

// AP Menu Event Handler
void vektor_remote_ap_menu_event(struct yolk *yk, void *u, uint8_t event)
{
	struct esp8266_ap *ap;

	// Check Event
	if((event != YOLK_MENU_EVENT_SELECT) && (event != YOLK_MENU_EVENT_RIGHT))													{ return; }

	// Fetch AP
	ap = &(vektor_remote_ap_menu_list[yolk_menu_selected_item_value(yk)]);

	// Inform
	yolk_dialog_enter(yk, 0, 0, YOLK_ICON_NET_UNKNOWN, "Please wait", "Connecting to %t", ap->name, ap->name_len);
	yolk_ui_draw(yk);

	// Connect
	if(esp8266_join_ap_n(ap->name, ap->name_len, 0, 0))																			{ yolk_dialog_enter_std_btn(yk, 0, YOLK_DIALOG_STD_BTN_OK, vektor_remote_ap_menu_retry, 0, 0, YOLK_ICON_NET_NONE, "Join failed", "Press Ok to retry"); return; }

	// Connect to AP
	if(socket_open_udp(wl0, &vektor_remote_sock, VEKTOR_REMOTE_ADDR, VEKTOR_REMOTE_PORT, 0, vektor_remote_sock_recv))			{ yolk_dialog_enter_std_btn(yk, 0, YOLK_DIALOG_STD_BTN_OK, vektor_remote_ap_menu_retry, 0, 0, YOLK_ICON_NET_NONE, "Cnct failed", "Press Ok to retry"); return; }

	// Enter Drive Dialog
	vektor_remote_drv_dialog_enter(yk, ap->name, ap->name_len);
}

/* Dooba SDK
 * Stickmote-based remote controller for the Vektor
 */

#ifndef	__VEKTOR_REMOTE_AP_MENU_H
#define	__VEKTOR_REMOTE_AP_MENU_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// AP List Size
#define	VEKTOR_REMOTE_AP_MENU_LIST_SIZE							20

// Enter AP Menu
extern void vektor_remote_ap_menu_enter(struct yolk *yk);

// Retry AP Menu
extern void vektor_remote_ap_menu_retry(struct yolk *yk, void *user);

// AP Menu Event Handler
extern void vektor_remote_ap_menu_event(struct yolk *yk, void *u, uint8_t event);

#endif

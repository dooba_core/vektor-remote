/* Dooba SDK
 * Stickmote-based remote controller for the Vektor
 */

#ifndef	__VEKTOR_REMOTE_DRV_DIALOG_H
#define	__VEKTOR_REMOTE_DRV_DIALOG_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Drive Dialog Active
extern uint8_t vektor_remote_drive_dialog_active;

// Enter Drive Dialog
extern void vektor_remote_drv_dialog_enter(struct yolk *yk, char *name, uint8_t name_len);

// Update Drive Dialog
extern void vektor_remote_drv_dialog_update(struct yolk *yk, void *u);

#endif
